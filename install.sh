#!/bin/sh

cwd=$(pwd)

trash=$HOME/trash
tools=$HOME/tools

[ -d "$trash" ] || mkdir $trash

[ "$1" = "reset" ] && { rm -rf $HOME/tools ; }

[ -d "$tools" ] || mkdir $tools


rm -f $tools/dotfiles
ln -s $(pwd) $tools/dotfiles


for f in *; do
	[ -d "$f" ] || continue

	case $f in
	*_dotfiles) 
		for ff in $f/*; do
			[ -e "$ff" ] || continue

echo "ff "  $ff
			bn=$(basename $ff)

            [ -z $bn ] && continue
			t="$trash"/$(date +"%Y%m%d%M%S")
			[ -d "$t" ] || mkdir -p  $t
			[ -e "$HOME/.$bn" ] && { mv $HOME/.$bn $t/ ; }
			rm -rf $HOME/.$bn
			ln -s 	$cwd/$ff $HOME/.$bn
		done
	;;
	dotdir_*) 
		bn=$(basename $f)
        	name=${bn##*_}
		rm -rf $HOME/.$name
		ln -s $cwd/$f $HOME/.$name
	;;
	*)
		t="$trash"/$(date +"%Y%m%d%M%S")
		[ -d "$t" ] || mkdir -p  $t
		[ -e "$HOME/.$f" ] && { mv $HOME/.$f $t/ ; }
		rm -rf $HOME/.$f 
        	ln -s 	$cwd/$f $HOME/.$f
	;;
	esac
done
